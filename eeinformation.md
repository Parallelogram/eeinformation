Everybody Edits Information
By Capasha (aka Doh)



### GameID and RoomType's

GameID              = "everybody-edits-su9rn58o40itdbnw69plyw"
Normalroom          = "Everybodyedits205"
Betaroom            = "Beta205"
Guestserviceroom    = "LobbyGuest205"
Lobby               = "Lobby205"
Blacklistroom       = "QuickInviteHandler205"
EmailRoom           = "ConfirmEmailHandler205"
Tutorialroom        = ["Tutorial205_world_1", "Tutorial205_world_2", "Tutorial205_world_3"]
Trackingroom        = "Tracking205"
Moderators & Admins = "ToolRoom"
CrewLobby           = "CrewLobby205"
ServiceRoom         = "$service-room$"

[Grab version from EE](http://pastebin.com/r2t7xnH6)

## Everybody Edits Message / PlayerIOClient.Message

### # init - Information about the room, width, height, blocks, owner and so on.
**[0]**  <String> RoomOwner  
**[1]**  <String> RoomTitle  
**[2]**  <Integer> RoomPlays  
**[3]**  <Integer> RoomFavorites  
**[4]**  <Integer> RoomLikes  
**[5]**  <String> RoomRot13  
**[6]**  <Integer> PlayerUserID  
**[7]**  <Integer> PlayerFace  
**[8]**  <Integer> PlayerAura  
**[9]**  <Integer> PlayerAuraColor < New  
**[10]** <Integer> PlayerPosX    
**[11]** <Integer> PlayerPosY  
**[12]** <Uint> PlayerChatColor  
**[13]** <String> PlayerUserName  
**[14]** <Boolean> PlayerCanEdit  
**[15]** <Boolean> PlayerIsOwner  
**[16]** <Boolean> PlayerHasFavorited  
**[17]** <Boolean> PlayerHasLiked  
**[18]** <Integer> WorldWidth  
**[19]** <Integer> WorldHeight  
**[20]** <Number> WorldGravityMultipier  
**[21]** <Uint> WorldBackgroundColor  
**[22]** <Boolean> WorldIsVisible  
**[23]** <Boolean> WorldIsHiddenFromLoby  
**[24]** <Boolean> WorldAllowsSpectating  
**[25]** <string> WorldDescription  
**[26]** <Integer> WorldCurseLimit  
**[27]** <Integer> WorldZombieLimit	  
**[28]** <Boolean> RoomIsCampaignRoom  
**[29]** <String> CrewId  
**[30]** <String> CrewName  
**[31]** <Boolean> PlayerCanChangeWorldOptions  
**[32]** <Integer> CrewWorldStatus  
**[33]** <String> PlayerBadge  
**[34]** <Boolean> PlayerISCrewMember  
**[35]** <Boolean> MapEnabled < New  
**[36]** <Boolean> LobbyPreviewEnabled	< New   
**[37]** <String> WorldStart/ws  
**[..]** RoomData starts here. You need to deserialize the message, to get it.  
**[..]** <String> WorldEnd/we

### # info - Message Box
**[0]** <String> Title  
**[1]** <String> Message

### # info2 - Messages
**[0]** <String> Title  
**[1]** <String> Body

### # write - Information from the server (* System, * Magic or * World)
**[0]** <String> Title  
**[1]** <String> Message

### # upgrade - EE has been updated, new version
	
### # copyPrompt - ?
**[0]** <String> Title  
**[1]** <String> Dtext  
**[2]** <String> ExtraText

### # psi - Switches
**[0]** <Integer> Userid  
**[1]** <ByteArray> SwitchId

### # ps - Switches
**[0]** <Integer> Userid  
**[1]** <Integer> SwitchId  
**[2]** <Integer> 0-1 (0 = off, 1 = on)
	
### # m - UserID is moving around
**[0]** <Integer> UserID  
**[1]** <Double> PlayerPosX  
**[2]** <Double> PlayerPosY  
**[3]** <Double> SpeedX  
**[4]** <Double> SpeedY  
**[5]** <Integer> ModifierX  
**[6]** <Integer> ModifierY  
**[7]** <Integer> Horizontal  
**[8]** <Integer> Vertical  
**[9]** <Boolean> SpaceDown  
**[10]** <Boolean> SpaceJustDown

### # Add - UserID joined the room
**[0]** <Integer> UserID  
**[1]** <String> UserName  
**[2]** <String> UserID (Like fb3213,kong3213, simple3213)  
**[3]** <Integer> SmileyID  
**[4]** <Double> PlayerPos X  
**[5]** <Double> PlayerPos Y  
**[6]** <Boolean> IsGod  
**[7]** <Boolean> IsAdmin  
**[8]** <Boolean> CanChat  
**[9]** <Integer> Coins  
**[10]** <Integer> BlueCoins  
**[11]** <Integer> Deaths  
**[12]** <Boolean> isFriendmember  
**[13]** <Boolean> isClubMember  
**[14]** <Boolean> ModeratorMode  
**[15]** <Integer> Team  
**[16]** <Integer> Aura  
**[17]** <Integer> ChatColor  
**[18]** <String> Badge   
    
    ========== Badges ==============
    adv 		Adventure League
    clr 		Colorful
    end 		Endurance
    ffs 		Fractured Fingers
    hlw 		Halloween
    pp1 		Puzzle Pack 1
    rns 		Ancient Ruins
    tnr 		Tunnel Rats
    ttr 		Tutorials
    wtr 		Winter
    ================================
**[19]** <Boolean> IsCrewMember

### # c - UserID hits a coin
**[0]** <Integer> UserID  
**[1]** <Integer> Yellow Coins  
**[2]** <Integer> Blue Coins  
**[3]** <Integer> TickID

### # kill - UserID gets killed
**[0]** <Integer> UserID

### # effect - UserID hits an effect
**[0]** <Integer> UserID  
**[1]** <Integer> Effect Id  
**[2]** <Boolean> Enabled/Disabled  
**[...]** <Integer> timeLeft/Number of Jumps  
**[...]** <Integer> Duration

    ======== Effect Id ===========	
    0			Jump
    1			Fly
    2			Speed
    3			Protection
    4			Curse
    5			Zombie
	9			MultiJump
    -	0 No Jumping (Number of Jumps)
    -	2 Two Jumps (Number of Jumps)
    ===============================
	
### # team - UserID joins a team
**[0]** <Integer> UserID  
**[1]** <Integer> Color  

    ======== Team Color ===========
    0 		Disabled
    1 		Red
    2 		Blue
    3 		Green
    4 		Cyan
    5 		Magenta
    6 		Yellow
    ===============================

### # k - UserID takes the crown
**[0]** <Integer> UserID

### # ks - UserID takes the Silver crown (trophy)
**[0]** <Integer> UserID

### # b - UserID added bricks, decorations, backgrounds (Blocks)
**[0]** <Integer> Layer (BackGround = 1, ForeGround = 0)  
**[1]** <Integer> PosX  
**[2]** <Integer> PosY  
**[3]** <Integer> BlockID  
**[4]** <Integer> UserID  

### # bc - Owner added a block that contains "goal"
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> BlockID  
**[3]** <Integer> CoinsToOpen  
**[4]** <Integer> UserID 

### # bs - Player added a noteblock
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> BlockID (77 = Piano, 83 = Drums)  
**[3]** <Integer> SoundID  
**[4]** <Integer> UserID 

### # pt - Owner added a portal
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> 242, 381  
**[3]** <Integer> Rotation (Down = 0, Left = 1, Up = 2, Right = 3)  
**[4]** <Integer> ID  
**[5]** <Integer> Target  
**[6]** <Integer> UserID 

### # lb - Admin added text
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> 1000  
**[3]** <String> Text  
**[4]** <String> TextColor  
**[5]** <Integer> UserID 

### # br - Player added rotation block
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> BlockID  
**[3]** <Integer> Rotation  
**[4]** <Integer> Layer  
**[5]** <Integer> UserID 

### # wp - Owner added World Portal
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> 374  
**[3]** <String> Text   
**[4]** <Integer> UserID 

### # ts - Owner added a Sign
**[0]** <Integer> PosX  
**[1]** <Integer> PosY  
**[2]** <Integer> 385  
**[3]** <String> Text   
**[4]** <Integer> UserID 

### # show - Keydoor get closed, gates gets opened.
**[0]** <String> Key Doors  

    ==== Key Doors ========
    Red
    Green
    Blue
    Cyan
    Magenta
    Yellow
    Timedoor
    ========================

### # hide - Keydoor gets opened, gates gets closed.
**[0]** <String> Key Doors  

    ==== Key Doors ========
    Red
    Green
    Blue
    Cyan
    Magenta
    Yellow
    Timedoor
    ========================

### # face - Someone changed smiley (Faces)
**[0]** <Integer> UserID  
**[1]** <Integer> Face

### # god - Someone toggled god mode
**[0]** <Integer> UserID  
**[1]** <Boolean> IsGod

### # admin - Someone toggled admin mode
**[0]** <Integer> UserID  
**[1]** <Boolean> isIndAdminMode

### # mod - Someone toggled moderator mode
**[0]** <Integer> UserID  
**[1]** <Boolean> isInModeratorMode

### # backgroundColor - The background color got changed
**[0]** <Boolean> IsEnabled  
**[..]** <Uint> Color

### # lostaccess - You lost access to the world password

### # access - You got password access to the world

### # say - UserID are saying something
**[0]** <Integer> UserID  
**[1]** <String> Text  
**[2]** <Boolean> FriendWithYou  
**[3]** <Uint> Color

### # say_old - Get the old chat messages (the messages when you join the room)
**[0]** <String> UserName  
**[1]** <String> Text

### # updatemeta - Owner changed the rooms titlename, or the plays/favorties/likes gets updated.
**[0]** <String> RoomOwner  
**[1]** <String> RoomTitle  
**[2]** <Integer> RoomPlays  
**[3]** <Integer> RoomFavorites  
**[4]** <Integer> RoomLIkes

### # autotext - UserID using the Quickchat, the "chat" for nonchatters.
**[0]** <Integer> UserID  
**[1]** <String> Text

### # left - UserID left the room
**[0]** <Integer> UserID

### # clear - Owner cleared the room
**[0]** <Integer> RoomWidth  
**[1]** <Integer> RoomHeight  
**[2]** <Integer> Border BlockID  
**[3]** <Integer> Workarea BlockID

### # tele - Owner reset the world
**[0]** <Boolean> True = used /reset command/Or teleported, False = Someone got killed  
**[1]** <Integer> UserID  
**[2]** <Integer> SpawnPosX  
**[3]** <Integer> SpawnPosY  
**[..]** If more users exists

### # reset - Owner loaded the world
**[..]** Includes RoomData

### # saved - Saved the world

### # liked - Liked the world

### # favorited - Favorited the world

### # unfavorited - Unfavorited the world

### # magic - Got magic, I guess.

### # banned - Got banned

### # completedLevel - Completed the level.

### # campaignRewards - Campaign Rewards.
**[0]** <Boolean> ShowBadge (Always true)  
**[1]** <String> BadgeTitle  
**[2]** <String> BadgeDescription  
**[3]** <String> BadgeImageUrl

### # campaignRewards - Campaign Rewards.
**[0]** <Boolean> ShowBadge (Always false)  
**[1]** <String> NextWorldUrl

### # toggleGod - Toggled God.
**[0]** <Integer> UserID  
**[1]** <Boolean> AllowToggle

### # editRights - Edit Rights.
**[0]** <Integer> UserID  
**[1]** <Boolean> HasEditRights

### # crewAddRequest - CrewAddRequest.
**[0]** <String> Username  
**[1]** <String> CrewName

### # crewAddRequestFailed - CrewAddRequestFailed.
**[0]** <String> Reason

### # addedToCrew - Added to Crew.
**[0]** <String> CrewID  
**[1]** <String> CrewName

### # worldReleased - WorldReleased.

### # aura
**[0]** <Integer> UserId  
**[1]** <Integer> Aura ID  
**[2]** <Integer> Aura Color  

    ===== Aura ID's ========
    0		Default
    1		Pinwheel
    2		Torus
    =======================
    ===== Aura Color ======
    0		White
    1		Red
    2		Blue
    3		Yellow
    4 		Green
    5		Purple
    6		Orange
    7 		Cyan
    =======================
	
### # muted
**[0]** <Integer> UserId  
**[1]** <Boolean> Muted

### # badgeChange
**[0]** <Integer> UserId  
**[1]** <String> Badge

### # restoreProgress
**[0]** <Integer> UserId  
**[1]** <Number> X  
**[2]** <Number> Y  
**[3]** <Integer> YellowCoins  
**[4]** <Integer> BlueCoins  
**[5]** <ByteArray> YellowCoinsTileX  
**[6]** <ByteArray> YellowCoinsTileY  
**[7]** <ByteArray> BlueCoinsTileX  
**[8]** <ByteArray> BlueCoinsTileY  
**[9]** <Integer> Deaths  
**[10]** <Uint> CheckPointX	 
**[11]** <Uint> CheckPointY  
**[12]** <ByteArray> Switches  
**[13]** <Number> SpeedX  
**[14]** <Number> SpeedY  
	
## Everybody Edits Send / Connection.Send() 

### # Portal
    <String> Rot13/Derot/Worldkey
    <Integer> 0
    <Integer> X
    <Integer> Y
    <Integer> BlockID (Portal = 242, Invisible Portal = 381)
    <Integer> Rotation (Down = 0, Left = 1, Up = 2, Right = 3, Invisible = 100)
    <Integer> ID
    <Integer> Target

### # Coin Door
    <String> Rot13/Derot/Worldkey
    <Integer> 0
    <Integer> X
    <Integer> Y
    <Integer> BlockID (Coindoor = 43, Coingate = 165)
    <Integer> Coins

### # Brick/Background (Blocks)
    <String> Rot13/Derot/Worldkey
    <Integer> Layer (Background = 1, Foreground = 0)
    <Integer> X
    <Integer> Y
    <Integer> BrickID

### # Coin
    <String> "c"
    <Integer> Yellow Coins To Collect
    <Integer> Blue Coins To Collect
    <Integer> X
    <Integer> Y

### # Say
    <String> "say"
    <String> Message

### # Keys
    <String> Rot13/Derot/Worldkey + "r", "g", "b", "c", "m", "y"
    <Integer> Position X
    <Integer> Position Y

### # Aura ID
    <String> "aura"  
    <Integer> Aura ID  
    <Integer> Aura Color
    ===== Aura ID's ========
    0		Default
    1		Pinwheel
    2		Torus
    =======================
    ===== Aura Color ======
    0		White
    1		Red
    2		Blue
    3		Yellow
    4 		Green
    5		Purple
    6		Orange
    7 		Cyan
    =======================
	
### # Touch diamond
    <String> "diamondtouch"
    <Integer> Position X
    <Integer> Position Y

### # Touch cake
    <String> "caketouch"
    <Integer> Position X
    <Integer> Position Y

### # Touch hologram
    <String> "hologramtouch"
    <Integer> Position X
    <Integer> Position Y

### # Touch checkpoint
    <String> "checkpoint"
    <Integer> Position X
    <Integer> Position Y

### # Touch Trophy
    <String> "levelcomplete"
    <Integer> Position X
    <Integer> Position Y

### # When you die
    <String> "death"

### # Activates zombie or curse effect
    <String> "timedeffect"
    <Integer> Effect
    <Integer> Duration
	
	====== Effect =========
	4		curse
	5		zombie
	=======================

### # Crown
    <String> Rot13/Derot/Worldkey + "k"
    <Integer> Position X
    <Integer> Position Y

### # Smiley (Faces)
    <String>Rot13/Derot/Worldkey + "f"
    <Integer> Face

### # Clear
    <String> "clear"

### # Save
    <String> "save"

### # God
    <String> "god"
    <Boolean> True or False

### # Room-Title
    <String> "name"
    <String> Title

### # Quickchat
    <String> "autosay"
    <Integer> Number
    
### # Get Edit Access
    <String> "access"
    <String> Password

### # Favorite
    <String> "favorite"

###    # Unfavorite
    <String> "unfavorite"

###  # Like
    <String> "like"

### # UnLike
    <String> "unlike"

### # Reject add to crew
    <String> "rejectAddToCrew"

### # Add to crew
    <String> "AddToCrew"

### # Request Add to Crew
    <String> "requestAddToCrew"
    <String> CrewID

### # Check Username
    <String> "checkUsername"
    <String> username

#### # Set Status  
<String> "setStatus"  
<Integer> status 

    0		WIP
    1		Open
    2		Released
    
### # Movement  
    
    <String> "m"
    <Double> PlayerPosX
    <Double> PlayerPosY
    <Double> SpeedX
    <Double> SpeedY
    <Integer> ModifierX
    <Integer> ModifierY
    <Integer> Horizontal
    <Integer> Vertical
    <Double> GravityMultiplier
    <Boolean> SpaceDown
    <Boolean> SpaceJustDown
    <Integer> TickID

### Misc
**Add basic grey block at position X 5 Y 5.**  
con.Send(Rot13/Derot/Worldkey,0,5,5,9);

**Add coin door at position X 5 and Y 5. And 10 coins to open it. (Owner Only)**  
con.Send(Rot13/Derot/Worldkey,0,5,5,43,10);

**Change face to :D**  
con.Send(Rot13/Derot/Worldkey/bldatam + "f", 1);

**Send private message to Username**  
con.Send("say","/pm Username Message");

**Respawn All players (Owner Only)**  
con.Send("say","/respawnall");

**Give toggleable god mode to username (Owner Only)**  
con.Send("say","/givegod Username");

**Give crown to username (Owner Only)**  
con.Send("say","/givecrown Username");

**Remove crown from anyone (Owner Only)**  
con.Send("say","/removecrown");

**Give god mode to username (Owner Only)**  
con.Send("say","/forcefly Username true");

**Remove god mode from username (Owner Only)**   
con.Send("say","/forcefly Username false");

**Give Edit Access (Owner Only)**  
con.Send("say","/giveedit Username");

**Take Edit Access from Username. (Owner Only)**  
con.Send("say","/removeedit Username");

**Kick a person from your room (Owner Only)**  
con.Send("say", "/kick Username Reason");

**Kill Username (Owner Only)**  
con.Send("say", "/kill Username");

**Reset the room (Owner Only)**  
con.Send("say", "/reset");

**Load the room (Owner Only)**  
con.Send("say", "/loadlevel");

**Clear the room (Owner Only)**  
con.Send("clear");

**Save the room (Owner Only)**  
con.Send("save");

**Set a password for the room (Owner Only)**  
con.Send("key","YourPassword");

**Set the title of the room (Owner Only)**  
con.Send("name","Welcome to my awesome world");

**Hide room from lobby and profile (true = on, false = off) (Owner Only)**  
con.Send("setHideLobby", true);

**Enable/Disable users to join the room (true = on, false = off) (Owner Only)**  
con.Send("setRoomVisible", true);

**Enable/Disable users to spectate other users (true = on, false = off) (Owner Only)**  
con.Send(""setAllowSpectating"", true);  
    
**Auto-text, text for registered users.**  
con.Send("autosay", 1);  

    1			Hi.
    2			Goodbye.
    3			Help me!
    4			Thank you.
    5			Follow me.
    6			Stop!
    7			Yes.
    8			No.
    9			Right.
    0			Left.
	
**Set background color to red hex color (Owner Only)**  
con.Send("say","/bgcolor #ff0000");

**Set background color to nothing (Owner Only)**  
con.Send("say","/bgcolor none");

### ShopData (getShop)
[Read how to use ShopData](http://pastebin.com/aZtrye2t)  

**[0]** <String> id ("brickspawn" / "smileylaughing" / "world0")  
**[1]** <String> type ("brick", "world", "club", "service", "crew")  
**[2]** <Integer> priceEnergy   
**[3]** <Integer> priceEnergyClick  
**[4]** <Integer> energyUsed  
**[5]** <Integer> pricegems  
**[6]** <Integer> owned_count  
**[7]** <Integer> span  
**[8]** <String> text_header  
**[9]** <String> text_body  
**[10]** <String> bitmapsheet_id  
**[11]** <Integer> bitmapsheet_offset  
**[12]** <Boolean> isOnSale  
**[13]** <Boolean> isFeatured  
**[14]** <Boolean> isClassic  
**[15]** <Boolean> isPlayerWorldOnly  
**[16]** <Boolean> isNew  
**[17]** <Boolean> isDevOnly  
**[18]** <Boolean> isGridFeatured  
**[19]** <Integer> priceUSD  
**[20]** <Boolean> reusable  
**[21]** <Integer> maxPurchases  
**[22]** <Boolean> ownedInPayvault  
**[23]** <String> LabelText